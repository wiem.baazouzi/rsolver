import { mount } from './app';
import CommandItem from './commandItem';
import Condition from './condition';
import DisambiguationItem from './disambiguationItem';
import { OutputMode } from './outputtable';
import Parser from './parser';
import { Response } from './searchEntities';
import UnaryCondition from './unaryCondition';
import Util from './util';
import TaskQueue from './taskQueue';

export type PropertyDictionary = { [ key: string ]: string };

export default class RequestTuple implements CommandItem {

	private _conditionList: Condition[];
	get conditionList(): Condition[] {
		return this._conditionList;
	}

	canLoadMore = false;

	continueFrom = 0;

	disambiguationItems: DisambiguationItem[] = [];

	propertyDict: PropertyDictionary;

	query: string;

	result: string = null;

	status: Status = Status.Pending;

	type: string;

	working = false;

	constructor( query: string, conditionList: Condition[], propertyDict: PropertyDictionary ) {
		this.query = query;
		this._conditionList = conditionList ?? [];
		this.propertyDict = propertyDict ?? {};

		const type = this.getProperty( 'type' )?.toLowerCase() ?? 'item';
		switch ( type ) {
		case 'f':
			this.type = 'form';
			break;
		case 'l':
			this.type = 'lexeme';
			break;
		case 'p':
			this.type = 'property';
			break;
		case 'q':
			this.type = 'item';
			break;
		case 's':
			this.type = 'sense';
			break;
		default:
			this.type = type;
		}
	}

	chooseDisambiguation( disambiguationItem: DisambiguationItem ): void {
		if ( disambiguationItem === null ) {
			this.result = null;
			this.status = this.disambiguationItems.length > 0 ? Status.Disambiguation : Status.NoResults;
		} else {
			this.result = disambiguationItem.entityId;
			this.status = Status.Success;
		}
	}

	editQuery( newTerm: string ): void {
		if ( mount.settings.renameKeepAsNote ) {
			const oldQuery = this.query;
			const oldNote = this.getProperty( 'note' );
			if ( oldNote === undefined ) {
				this.setProperty( 'note', oldQuery );
			} else {
				this.setProperty( 'note', `${oldQuery}; ${oldNote}` );
			}
		}

		this.query = newTerm;
		this.result = null;
		this.status = Status.Pending;
	}

	finaliseRequest( items: any[], append = false ): void {
		if ( this.status !== Status.Removed ) {
			if ( items.length === 1 ) {
				this.status = Status.Success;
				this.result = items[ 0 ].id;
			} else if ( items.length === 0 ) {
				this.status = Status.NoResults;
				this.result = null;
			} else {
				this.status = Status.Disambiguation;
			}

			const disambiguationItems = items.map( ( disambiguationItem ) => {
				return new DisambiguationItem( disambiguationItem.id, disambiguationItem.label, disambiguationItem.description, disambiguationItem );
			} );

			if ( append ) {
				this.disambiguationItems = [ ...this.disambiguationItems, ...disambiguationItems ];
			} else {
				this.disambiguationItems = disambiguationItems;
			}
		}
	}

	getOutput( mode: OutputMode ): string {
		switch ( mode ) {
		case OutputMode.Failed:
			if ( ( this._conditionList === null || this._conditionList.length === 0 ) && ( this.propertyDict === null || Object.keys( this.propertyDict ).length === 0 ) ) {
				return Parser.encodeSearchQuery( this.query );
			} else {
				const conditions = ( this._conditionList === null ? '' : this._conditionList.map( ( condition ) => condition.getOutput( mode ) ).join( ';' ) );
				const properties = ( this.propertyDict ) === null || Object.keys( this.propertyDict ).length === 0 ? null : Object.keys( this.propertyDict ).map( ( propertyKey ) => {
					return `${Parser.encodeSearchQuery( propertyKey )}=${Parser.encodeSearchQuery( this.propertyDict[ propertyKey ] )}`;
				} ).join( ';' );

				if ( properties === null ) {
					return `${Parser.encodeSearchQuery( this.query )}|${conditions}`;
				} else {
					return `${Parser.encodeSearchQuery( this.query )}|${conditions}|${properties}`;
				}
			}
		case OutputMode.Success:
		default:
			return this.result;
		}
	}

	getOutputStatus(): OutputMode {
		if ( this.result === null ) {
			return OutputMode.Failed;
		} else {
			return OutputMode.Success;
		}
	}

	/**
	 * Get the value of one of the tuple's properties.
	 *
	 * @param {string} key Key to lookup.
	 * @return {string} Value associated with the key, or `undefined`.
	 */
	getProperty( key: string ): string {
		return this.propertyDict[ key ];
	}

	reportResultCount( count: number, limit: number ): void {
		this.continueFrom += count;
		this.canLoadMore = count >= limit;
	}

	async request( taskQueue: TaskQueue ): Promise<void> {
		if ( this._conditionList.length === 1 && this._conditionList[ 0 ].operator === '^' ) {
			const host = Util.getWikiHost( ( <UnaryCondition> this._conditionList[ 0 ] ).operand );
			if ( host !== null ) {
				await taskQueue.enqueueTask( Util.PRIORITY_MEDIAWIKI_API );
				await this._conditionList[ 0 ].getRequest( taskQueue, this );
				this.working = false;
			} else {
				this.status = Status.Failed;
				this.working = false;
				throw new Error();
			}
		} else {
			const statusReporter = await taskQueue.enqueueTask( Util.PRIORITY_WIKIDATA_API_REQUESTTUPLE );

			this.status = Status.Working;
			this.working = true;
			const limit = mount.settings.disambiguationCandidatesNumber;

			try {
				const data: Response = await $.ajax( {
					data: {
						action: 'wbsearchentities',
						format: 'json',
						language: mount.settings.searchLanguage,
						limit: limit,
						origin: '*',
						search: this.query,
						type: this.type,
						uselang: mount.settings.searchLanguage
					},
					url: 'https://www.wikidata.org/w/api.php'
				} );
				statusReporter.finishTask();

				if ( data.success !== 1 ) {
					throw new Error( 'API returned error' );
				}

				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-ignore 2367
				if ( this.status !== Status.Removed ) {
					this.reportResultCount( data.search.length, limit );
					if ( data.search.length === 0 ) {
						this.status = Status.NoResults;
					} else {
						const items = await Condition.filterItems( taskQueue, this, data.search, mount.settings.itemDescriptionLanguage );
						this.finaliseRequest( items );
					}
				}
				this.working = false;
			} catch ( ex ) {
				statusReporter.finishTask();

				this.status = Status.Failed;
				this.working = false;
				throw ex;
			}
		}
	}

	setProperty( property: string, value: string ): void {
		this.propertyDict[ property ] = value;
	}

	async requestMore( taskQueue: TaskQueue ): Promise<void> {
		const statusReporter = await taskQueue.enqueueTask( Util.PRIORITY_WIKIDATA_API_REQUESTTUPLE );

		this.canLoadMore = false;
		this.status = Status.Working;
		this.working = true;
		const limit = mount.settings.disambiguationCandidatesNumber;

		try {
			const data: Response = await $.ajax( {
				data: {
					action: 'wbsearchentities',
					continue: this.continueFrom,
					format: 'json',
					language: mount.settings.searchLanguage,
					limit: limit,
					origin: '*',
					search: this.query,
					type: this.type,
					uselang: mount.settings.searchLanguage
				},
				url: 'https://www.wikidata.org/w/api.php'
			} );
			statusReporter.finishTask();

			if ( data.success !== 1 ) {
				throw new Error( 'API returned error' );
			}

			// eslint-disable-next-line @typescript-eslint/ban-ts-comment
			// @ts-ignore 2367
			if ( this.status !== Status.Removed ) {
				this.reportResultCount( data.search.length, limit );
				if ( data.search.length === 0 ) {
					this.status = Status.NoResults;
				} else {
					const items = await Condition.filterItems( taskQueue, this, data.search, mount.settings.itemDescriptionLanguage );
					this.finaliseRequest( items, true );
				}
			}
			this.working = false;
		} catch ( ex ) {
			statusReporter.finishTask();

			this.status = Status.Failed;
			this.working = false;
			throw ex;
		}
	}
}

// eslint-disable-next-line no-shadow
export enum Status {
	// eslint-disable-next-line no-unused-vars
	Disambiguation = 'disambiguation',
	// eslint-disable-next-line no-unused-vars
	Failed = 'failed',
	// eslint-disable-next-line no-unused-vars
	Filtering = 'filtering',
	// eslint-disable-next-line no-unused-vars
	NoResults = 'no results',
	// eslint-disable-next-line no-unused-vars
	Pending = 'pending',
	// eslint-disable-next-line no-unused-vars
	Removed = 'removed',
	// eslint-disable-next-line no-unused-vars
	Success = 'success',
	// eslint-disable-next-line no-unused-vars
	Working = 'working'
}
