import { mount } from './app';

// eslint-disable-next-line no-undef
let outputPipe: MessageEventSource = null;
let outputPipeTargetOrigin: string = null;

export function pipeOutput( outputSuccess: string, outputFailed: string ): void {
	outputPipe.postMessage( {
		type: 'rsolver.outputPipe',
		data: {
			failed: outputFailed,
			success: outputSuccess
		}
	}, {
		targetOrigin: outputPipeTargetOrigin
	} );
}

export function setupApiListener(): void {
	window.addEventListener( 'message', ( e ) => {
		const data = e.data;
		outputPipe = e.source;
		outputPipeTargetOrigin = e.origin;

		switch ( data.type ) {
		case 'outputPipeEnable':
			mount.output.pipeEnabled = true;
			e.source.postMessage( {
				type: 'rsolver.outputPipeRegistered'
			}, {
				targetOrigin: outputPipeTargetOrigin
			} );
			break;
		case 'setInput':
			mount.input = data.value;
			break;
		default:
			console.error( `Received unknown message of type ${data.type}`, data );
		}
	}, false );
}
