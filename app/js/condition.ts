import { mount } from './app';
import Outputtable, { OutputMode } from './outputtable';
import RequestTuple from './requestTuple';
import { Status } from './requestTuple';
import { Result } from './searchEntities';
import TaskQueue from './taskQueue';
import Util from './util';

export default abstract class Condition implements Outputtable {
	operator: string;

	protected constructor( operator: string ) {
		this.operator = operator;
	}

	static extendData( oldItem: any, newItem: any, langCode: string ): void {
		if ( newItem.descriptions[ langCode ] !== undefined ) {
			oldItem.description = newItem.descriptions[ langCode ].value;
		}
		if ( newItem.labels[ langCode ] !== undefined ) {
			oldItem.label = newItem.labels[ langCode ].value;
		}
	}

	static async filterItem( _taskQueue: TaskQueue, tuple: RequestTuple, elements: any[], results: any, langCode: string ): Promise<any[]> {
		const elementsNew = elements.slice( 0 );
		tuple.status = Status.Pending;

		const statusReporter = await _taskQueue.enqueueTask( Util.PRIORITY_WIKIDATA_QUERY_SERVICE );
		tuple.status = Status.Filtering;

		try {
			const resultIds = Object.keys( results.entities ).map( ( wdId ) => `wd:${wdId}` );
			const data = await $.ajax( {
				beforeSend: function ( request ) {
					request.setRequestHeader( 'Accept', 'application/json, text/plain, */*' );
				},
				data: {
					query: `SELECT ?item ?itemLabel WHERE { VALUES ?item {${resultIds.join( ' ' )}} ${generateConditions().join( '' )} SERVICE wikibase:label { bd:serviceParam wikibase:language "${langCode},en". } }`
				},
				type: 'GET',
				url: 'https://query.wikidata.org/sparql'
			} );
			statusReporter.finishTask();
			const sparqlMatches = data.results.bindings.map( ( binding: any ) => {
				return binding.item.value.replace( 'http://www.wikidata.org/entity/', '' );
			} );

			for ( const [ index, candidate ] of elements.entries() ) {
				if ( sparqlMatches.includes( candidate.id ) ) {
					Condition.extendData( elementsNew[ index ], results.entities[ candidate.id ], langCode );
				} else {
					elementsNew[ index ] = null;
				}
			}

			return elementsNew.filter( ( n ) => { // Filter out 'null'
				return n !== null;
			} );
		} catch ( ex ) {
			console.error( ex );
			statusReporter.finishTask();

			return elements;
		}

		function generateConditions() {
			return tuple.conditionList.filter( ( condition ) => {
				return condition.generateCondition() !== null;
			} ).map( ( condition ) => {
				return condition.generateCondition();
			} );
		}
	}

	static async filterItems( taskQueue: TaskQueue, tuple: RequestTuple, elements: Result[], langCode: string ): Promise<any[]> {
		const ids: string[] = elements.map( ( element ) => element.id );

		const statusReporter = await taskQueue.enqueueTask( Util.PRIORITY_WIKIDATA_API_CONDITION );
		try {
			const data = await $.ajax( {
				data: {
					action: 'wbgetentities',
					format: 'json',
					languages: `${mount.settings.searchLanguage.toLowerCase()}|en`,
					origin: '*',
					ids: ids.join( '|' )
				},
				url: 'https://www.wikidata.org/w/api.php'
			} );
			statusReporter.finishTask();

			if ( tuple.conditionList.length > 0 ) {
				const items = await Condition.filterItem( taskQueue, tuple, elements, data, langCode );
				return items;
			} else {
				return elements;
			}
		} catch {
			statusReporter.finishTask();

			return elements;
		}
	}

	abstract generateCondition(): string;

	abstract getOutput( mode: OutputMode ): string;

	abstract getRequest( taskQueue: TaskQueue, tuple: RequestTuple ): Promise<void>;
}
