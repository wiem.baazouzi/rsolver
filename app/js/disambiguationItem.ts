export default class DisambiguationItem {

	description: string;

	entityId: string;

	label: string;

	raw: any;

	constructor( entityId: string, label: string, description: string, raw: any ) {
		this.description = description;
		this.entityId = entityId;
		this.label = label;
		this.raw = raw;
	}
}
