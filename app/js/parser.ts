import BinaryCondition from './binaryCondition';
import Literal from './literal';
import Peggy from 'peggy';
import RequestTuple from './requestTuple';
import UnaryCondition from './unaryCondition';

export default class Parser {
	// eslint-disable-next-line no-undef
	private static _grammar: string = require( '../data/grammar.pegjs' ).default;
	static get grammar(): string {
		return Parser._grammar;
	}

	private parser: Peggy.Parser;

	constructor() {
		this.parser = Peggy.generate( Parser._grammar );
	}

	static encodeSearchQuery( input: string ): string {
		const rules: { [ char: string ] : string } = {
			'=': '\\=',
			'~': '\\~',
			'^': '\\^',
			';': '\\;',
			'\\': '\\\\',
			'|': '\\|'
		};

		return input.split( '' ).map( ( char ) => typeof rules[ char ] !== 'undefined' ? rules[ char ] : char ).join( '' );
	}

	parse( input: string ): any {
		return this.parser.parse( input, {
			BinaryCondition: BinaryCondition,
			Literal: Literal,
			RequestTuple: RequestTuple,
			UnaryCondition: UnaryCondition
		} );
	}
}
