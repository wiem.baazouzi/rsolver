import CommandItem from './commandItem';
import { OutputMode } from './outputtable';

export default class Literal implements CommandItem {
	literal: string;

	constructor( literal: string ) {
		this.literal = literal;
	}

	getOutput( mode: OutputMode ): string {
		switch ( mode ) {
		case OutputMode.Failed:
			return `$${this.literal}`;
		case OutputMode.Success:
		default:
			return this.literal;
		}
	}

	getOutputStatus(): OutputMode {
		return OutputMode.Success;
	}
}
